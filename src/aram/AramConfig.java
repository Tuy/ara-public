package aram;

import peersim.config.*;


public class AramConfig {
	private static double singleMessage;
	private static double broadcastMessage;
	private static int maxIncr;
	private static int minIncr;
	private static int minBackupDelay;
	private static int maxBackupDelay;
	private static double failureProb;
	private static int maxCountFailure;
    private static int timeoutRecov;
    private static boolean graphicsEnabled;
    private static boolean appMsgEnabled;
    private static boolean rollMsgEnabled;
    private static boolean rollbackEnabled;
    private static boolean paused = false;
    private static int usematrix;
    private static int pace = 0;
    private static int hbtimeout;
    private static int timebetweenhbs;
    private static int withHB;
    private static int hideHB;
	
	public static void loadConfig(){
		singleMessage = Configuration.getDouble("aram.singleMessage");
		broadcastMessage = Configuration.getDouble("aram.broadcastMessage");
		maxIncr = Configuration.getInt("aram.maxIncr");
		minIncr = Configuration.getInt("aram.minIncr");
		maxBackupDelay = Configuration.getInt("aram.maxBackupDelay");
		minBackupDelay = Configuration.getInt("aram.minBackupDelay");
		failureProb = Configuration.getDouble("aram.failureProb");
		maxCountFailure = Configuration.getInt("aram.maxCountFailure");
        timeoutRecov = Configuration.getInt("aram.timeoutRecov");
        graphicsEnabled = Configuration.getBoolean("aram.graphics");
        usematrix = Configuration.getInt("aram.usematrix");
        hbtimeout = Configuration.getInt("aram.hbtimeout");
        timebetweenhbs = Configuration.getInt("aram.timebetweenhbs");
        withHB = Configuration.getInt("aram.withHB");
        hideHB = Configuration.getInt("aram.hideHB");
        
        if (graphicsEnabled){
        	appMsgEnabled = Configuration.getBoolean("aram.appMsgs");
        	rollMsgEnabled = Configuration.getBoolean("aram.rollMsgs");
        	rollbackEnabled = Configuration.getBoolean("aram.rollback");
        }
        else {
        	appMsgEnabled = false;
        	rollMsgEnabled = false;
        	rollbackEnabled = false;
        }
        
	}

	public static double getSingleMessage() {
		return singleMessage;
	}
	public static double getBroadcastMessage() {
		return broadcastMessage;
	}
	public static int getMaxIncr() {
		return maxIncr;
	}
	public static int getMinIncr() {
		return minIncr;
	}
	public static int getMinBackupDelay() {
		return minBackupDelay;
	}
	public static int getMaxBackupDelay() {
		return maxBackupDelay;
	}
	public static double getFailureProb() {
		return failureProb;
	}
	public static int getMaxCountFailure() {
		return maxCountFailure;
	}
    public static int getTimeoutRecov(){
        return timeoutRecov;
    }

	public static boolean isGraphicsEnabled() {
		return graphicsEnabled;
	}

	public static boolean isAppMsgEnabled() {
		return appMsgEnabled;
	}

	public static boolean isRollMsgEnabled() {
		return rollMsgEnabled;
	}

	public static boolean isRollbackEnabled() {
		return rollbackEnabled;
	}

	public static boolean isPaused() {
		return paused;
	}
	
	public static int getPace() {
		return pace;
	}

	public static void pause() {
		paused = !paused;
	}
    public static int getUsematrix(){
        return usematrix;
    }
	
	public static void speedUp(){
		if(pace >= 100)
			pace -= 100;
	}
	
	public static void slowDown(){
		if(pace <= 900){
			pace += 100;
		}
	}
    public static int getHbtimeout(){
        return hbtimeout;
    }
    public static int getTimebetweenhbs(){
        return timebetweenhbs;
    }
    public static int getWithHB(){
        return withHB;
    }
    public static int getHideHB(){
        return hideHB;
    }
    
}
