package aram;

import peersim.edsim.*;
import peersim.core.*;

public class AppState {

    private int state;
    private int countCheckpoints;
    private int[] recvCounter;
    private int[] sentCounter;
    private Graphics frame;
    
    AppState(int networkSize) {
        this.state = 0;
        this.countCheckpoints = 0;
        this.recvCounter = new int[networkSize];
        this.sentCounter = new int[networkSize];
        for (int i=0; i<networkSize; i++){
        	this.recvCounter[i] = 0;
        	this.sentCounter[i] = 0;
        }
        this.frame = null;
    }

    public int getState(){
        return this.state;
    }
    public void setState(int s){
        this.state = s;
    }
    public void incrState(){
    	this.state++;
    }    
    public void incrState(int proc, int time){
    	this.state++;
    	frame.addState(proc, time, state);
    }
    public int getCountCheckpoints(){
        return this.countCheckpoints;
    }
	public int[] getRecvCounter() {
		return recvCounter;
	}
	public void setRecvCounter(int[] recvCounter) {
		this.recvCounter = recvCounter;
	}
	public void incrRecvCounter(int id){
		this.recvCounter[id]++;
	}
	public int[] getSentCounter() {
		return sentCounter;
	}
	public void setSentCounter(int[] sentCounter) {
		this.sentCounter = sentCounter;
	}
	public void incrSentCounter(int id){
		this.sentCounter[id]++;
	}
	public void setCountCheckpoints(int countCheckpoints) {
		this.countCheckpoints = countCheckpoints;
	}
	public void incrCountCheckpoints(){
		this.countCheckpoints++;
	}
	
	public String toString() {
        return "" + this.state;
    }

	public void setFrame(Graphics frame) {
		this.frame = frame;
	}
    

}

