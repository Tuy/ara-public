package aram;

import peersim.edsim.*;

public class Message {

    public final static int INCRSTATE = 0;
    public final static int APPMSG = 1;
    public final static int DOCHECKPOINT = 2;
    public final static int FAILURE = 3;
    public final static int ROLLBACK = 4;
    public final static int ENDRECOV = 5;
    public final static int HEARTBEAT = 6;
    public final static int WAKEUP = 7;
    public final static int SENDHEARTBEATS = 8;
    public final static int CHECKTM = 9;
    public final static int GETDOWN = 10;

    private int type;
    private String content;
    private int senti;
    private int emitter;
    private Graphics fr;
    
    Message(int type, String content, int emitter) {
        this.type = type;
        this.content = content;
        this.senti = -1;
        this.emitter = emitter;
        this.fr = null;
    }

    Message(Message m){
        this.type = m.getType();
        this.content = new String(m.getContent());
        this.senti = m.getSenti();
        this.emitter = m.getEmitter();
        this.fr = null;
    }

    public String getContent() {
        return this.content;
    }
    public int getType() {
        return this.type;
    }
	public int getSenti() {
		return senti;
	}
	public void setSenti(int senti) {
		this.senti = senti;
	}
	public int getEmitter() {
		return emitter;
	}
	public void setEmitter(int emitter) {
		this.emitter = emitter;
	}
	public Graphics getFrame(){
		return fr;
	}
	public void setFrame(Graphics fr){
		this.fr = fr;
	}
}
