package aram;

import java.util.LinkedList;

import peersim.edsim.*;
import peersim.core.*;
import peersim.config.*;

public class AramApp implements EDProtocol {

    //identifiant de la couche transport
    private int transportPid;

    //objet couche transport
    private Protocol transport;

    //identifiant de la couche courante (la couche applicative)
    private int mypid;

    //le numero de noeud
    private int nodeId;

    //prefixe de la couche (nom de la variable de protocole du fichier de config)
    private String prefix;

    // Etat du noeud & variables internes
    private AppState state;
    private int[] recvedHB;
    private LinkedList<Checkpoint> checkpointsList = new LinkedList<Checkpoint>();
    private boolean isRecovering = false;
    private boolean isDoCheckpointStopped = false;
    private boolean isIncrStateStopped = false;
    private long expirationRecover = 0;
    private int nbWakeupRecvd = 0;
    private Graphics frame;

    
    public AramApp(String prefix) {
        this.prefix = prefix;
        // Initialisation des identifiants a partir du fichier de configuration
        this.transportPid = Configuration.getPid(prefix + ".transport");
        this.mypid = Configuration.getPid(prefix + ".myself");
        this.transport = null;
        AramConfig.loadConfig();
        state = new AppState(Network.size());
        frame = null;
        this.recvedHB = new int[Network.size()];
        for (int i=0; i<Network.size(); i++)
            this.recvedHB[i] = 0;
    }

    // Methode appelee lorsqu'un message est recu par le protocole AramApp du noeud
    public void processEvent(Node node, int pid, Object event ) {
        this.receive((Message)event);
    }

    // Methode necessaire pour la creation du reseau (qui se fait par clonage d'un prototype)
    public Object clone() {
        AramApp dolly = new AramApp(this.prefix);
        return dolly;
    }

    /* Liaison entre un objet de la couche applicative et un 
    * objet de la couche transport situes sur le meme noeud */
    public void setTransportLayer(int nodeId) {
        this.nodeId = nodeId;
        if (AramConfig.getUsematrix() == 1){
            this.transport = (MatrixTransport)Network.get(this.nodeId).getProtocol(this.transportPid);
        } else {
            this.transport = (HWTransport)Network.get(this.nodeId).getProtocol(this.transportPid);
        }
    }

    // Methode d'envoi d'un message (l'envoi se fait via la couche transport)
    public void send(Message msg, Node dest) {
        // On affiche les mesages qu'on envoie sauf si l'affichage des hb est desactive
        if (AramConfig.getHideHB() == 0 || msg.getType() != Message.HEARTBEAT) 
            System.out.println("["+CommonState.getTime()+"::"+this+"] SEND TO "+dest.getID()+" -> Type: "+msg.getType()+" -- State: "+this.state);
        // S'il s'agit d'un message applicatif on incrémente le compteur
        if (msg.getType() == Message.APPMSG){
        	this.state.incrSentCounter((int)dest.getID());
        	if (AramConfig.isAppMsgEnabled())
        		frame.sentAppMsg(this.nodeId, (int)dest.getID(), (int) CommonState.getTime(), this.state.getSentCounter()[(int)dest.getID()]);
        }
        // Intégration du rollback dans la partie graphique
        if (msg.getType() == Message.ROLLBACK){
        	if (AramConfig.isRollMsgEnabled())
        		frame.sentRollMsg(this.nodeId, (int)dest.getID(), (int) CommonState.getTime());
        	else if (AramConfig.isRollbackEnabled())
        		frame.addRollback(this.nodeId, (int) CommonState.getTime(), this.state.getCountCheckpoints());
        }
        // Suivant la couche de transport choisie on envoie le message
        if (AramConfig.getUsematrix() == 1){
            ((MatrixTransport)this.transport).send(getMyNode(), dest, msg, this.mypid);
        } else {
            ((HWTransport)this.transport).send(getMyNode(), dest, msg, this.mypid);
        }
    }

    // Methode de broadcast
    public void broadcast(Message m){
        for (int i=1; i<Network.size(); i++){	
            if (Network.get(i) != this.getMyNode()){
                Message msg = new Message(m);
                this.send(msg, Network.get(i));
            }
        }
    }

    // Methode traitant la reception d'un message
    private void receive(Message msg) {
        // On transforme le numero du type en un nom lisible
    	String mType;
    	if (msg.getType() == Message.APPMSG)
    		mType = "APPMSG";
    	else if (msg.getType() == Message.DOCHECKPOINT)
    		mType = "DOCHECKPOINT";
    	else if (msg.getType() == Message.INCRSTATE)
    		mType = "INCRSTATE";
    	else if (msg.getType() == Message.FAILURE)
    		mType = "FAILURE";
    	else if (msg.getType() == Message.ROLLBACK)
    		mType = "ROLLBACK";
        else if (msg.getType() == Message.HEARTBEAT)
            mType = "HEARTBEAT";
        else if (msg.getType() == Message.WAKEUP)
            mType = "WAKEUP";
    	else
    		mType = "UNKNOWN";
    	
        // Integration des checkpoints avec la partie graphique
    	if (msg.getType() == Message.DOCHECKPOINT && msg.getFrame() != null){
    		frame = msg.getFrame();
    		state.setFrame(frame);
    	}
    
        // Affichage du message recu sauf s'il s'agit d'un message interne    
        if (msg.getType() != Message.SENDHEARTBEATS && msg.getType() != Message.ENDRECOV && msg.getType() != Message.CHECKTM){
            if (AramConfig.getHideHB() == 0 || msg.getType() != Message.HEARTBEAT) 
                System.out.println("["+CommonState.getTime()+"::"+this+"] RECEPTION FROM "+msg.getEmitter()+" -> Type: "+mType+" -- State: "+this.state);
    	}

    	// On incremente le compteur de reception si c'est un message applicatif
    	if (msg.getEmitter() != this.nodeId && msg.getType() == Message.APPMSG){
    		this.state.incrRecvCounter(msg.getEmitter());
    		if(AramConfig.isAppMsgEnabled())
    			frame.recvdAppMsg(msg.getEmitter(), this.nodeId, (int) CommonState.getTime(), this.state.getRecvCounter()[msg.getEmitter()]);
    	}
    
        // Cas du message interne faisant incrementer l'etat
        // Et tirage aleatoire pour l'envoie/broadcast de messages    
        if (msg.getType() == Message.INCRSTATE && (this.isIncrStateStopped = true) && !this.isRecovering){
            this.isIncrStateStopped = false;
            // On incremente state
        	if(AramConfig.isGraphicsEnabled())
        		this.state.incrState(this.nodeId, (int) CommonState.getTime());
        	else
        		this.state.incrState();

            // On positionne la prochaine incrementation
            int nextIncr = CommonState.r.nextInt(AramConfig.getMaxIncr()-AramConfig.getMinIncr()) + AramConfig.getMinIncr();
            Message incrMsg = new Message(Message.INCRSTATE, "", this.nodeId);
            EDSimulator.add(nextIncr, incrMsg, this.getMyNode(), this.mypid);

            // Tirage pour message single
            if(CommonState.r.nextDouble() < AramConfig.getSingleMessage()){
                Message appmsg = new Message(Message.APPMSG, "", this.nodeId);
                int randomNode = CommonState.r.nextInt(Network.size()-1)+1;  
                this.send(appmsg, Network.get(randomNode)); 
            }

            // Tirage pour broadcast
            if(CommonState.r.nextDouble() < AramConfig.getBroadcastMessage()){
               Message appmsg = new Message(Message.APPMSG, "", this.nodeId);
               this.broadcast(appmsg); 
            }
        
        // Cas du message interne demandant l'execution d'un checkpoint
        } else if (msg.getType() == Message.DOCHECKPOINT && (this.isDoCheckpointStopped = true) && !this.isRecovering ){
            this.isDoCheckpointStopped = false;
        	if(AramConfig.isGraphicsEnabled())
        		CheckpointUtil.doCheckPoint(checkpointsList, state, (int)CommonState.getTime(), frame, this.nodeId);
        	else
        		CheckpointUtil.doCheckPoint(checkpointsList, state, (int)CommonState.getTime());
            if (CommonState.getTime() != 0){
                Message cpMsg = new Message(Message.DOCHECKPOINT, "", this.nodeId);
                EDSimulator.add(CommonState.r.nextInt(AramConfig.getMaxBackupDelay()-AramConfig.getMinBackupDelay()) + AramConfig.getMinBackupDelay(), cpMsg, Network.get(this.nodeId), this.mypid);
            }
        
        // Cas du message interne demandant un recouvrement immediat au dernier checkpoint
        } else if (msg.getType() == Message.FAILURE){
        	this.isRecovering = true;

            // On positionne le timeout du recover
            this.expirationRecover = CommonState.getTime() + AramConfig.getTimeoutRecov();
            Message endRecovMsg = new Message(Message.ENDRECOV, "", this.nodeId);
            EDSimulator.add(AramConfig.getTimeoutRecov(), endRecovMsg, this.getMyNode(), this.mypid);
        	
        	if(AramConfig.isGraphicsEnabled())
        		frame.failed(this.nodeId, (int) CommonState.getTime());
        
            // On lance le rollback et on envoie un message à tout le monde    
        	CheckpointUtil.rollback(checkpointsList, this.state);
        	System.out.println("########## INIT ROLLBACK ("+this.nodeId+") ########## ");
        	for (int i=1; i<Network.size(); i++){
        		if (i != this.nodeId){
                    Message recoverMsg = new Message(Message.ROLLBACK, "", this.nodeId);
        			recoverMsg.setSenti(this.state.getSentCounter()[i]);
        			this.send(recoverMsg, Network.get(i));
        		}
        	}
        
        // Cas du message prevenant d'un rollback    
        } else if (msg.getType() == Message.ROLLBACK){
    		if(AramConfig.isRollMsgEnabled())
    			frame.recvdRollMsg(msg.getEmitter(), this.nodeId, (int) CommonState.getTime());
        	
            this.isRecovering = true;
            
            // On positionne le timeout du recover
            this.expirationRecover = CommonState.getTime() + AramConfig.getTimeoutRecov();
            Message endRecovMsg = new Message(Message.ENDRECOV, "", this.nodeId);
            EDSimulator.add(AramConfig.getTimeoutRecov(), endRecovMsg, this.getMyNode(), this.mypid);
        	
            // On regarde si on doit rollback nous meme ou non
        	if (msg.getSenti() < this.state.getRecvCounter()[msg.getEmitter()]){
        		if(AramConfig.isGraphicsEnabled())
        			CheckpointUtil.rollbackWhere(checkpointsList, this.state, msg.getEmitter(), msg.getSenti(), frame);
        		else
        			CheckpointUtil.rollbackWhere(checkpointsList, this.state, msg.getEmitter(), msg.getSenti());
        		System.out.println("########## ROLLINGBACK ("+this.nodeId+") ########## ");
            	for (int i=1; i<Network.size(); i++){
            		if (i != this.nodeId){
                        Message recoverMsg = new Message(Message.ROLLBACK, "", this.nodeId);
            			recoverMsg.setSenti(this.state.getSentCounter()[i]);
            			this.send(recoverMsg, Network.get(i));
            		}
            	}
        	}

        // Cas du message interne prevenant de l'expiration de la phase de rollback
        } else if (msg.getType() == Message.ENDRECOV){
            if (this.expirationRecover == CommonState.getTime() && this.isRecovering){
                this.isRecovering = false;
                // On positionne la prochaine incrementation pour relancer le state
                if (this.isIncrStateStopped){
                    int nextIncr = CommonState.r.nextInt(AramConfig.getMaxIncr()-AramConfig.getMinIncr()) + AramConfig.getMinIncr();
                    Message incrMsg = new Message(Message.INCRSTATE, "", this.nodeId);
                    EDSimulator.add(nextIncr, incrMsg, this.getMyNode(), this.mypid);
                }
                // On fait de meme avec le checkpoint
                if (this.isDoCheckpointStopped){
                    Message cpMsg = new Message(Message.DOCHECKPOINT, "", this.nodeId);
                    EDSimulator.add(CommonState.r.nextInt(AramConfig.getMaxBackupDelay()-AramConfig.getMinBackupDelay()) + AramConfig.getMinBackupDelay(), cpMsg, Network.get(this.nodeId), this.mypid);
                }
            }

        // Cas du message interne indiquant l'envoie des heartbeats
        } else if (msg.getType() == Message.SENDHEARTBEATS){
            Message shb = new Message(Message.SENDHEARTBEATS, "", this.nodeId);
            Message hbm = new Message(Message.HEARTBEAT, "", this.nodeId); 
            EDSimulator.add(AramConfig.getTimebetweenhbs(), shb, this.getMyNode(), this.mypid);
            this.broadcast(hbm);

        // Cas de la reception d'un heartbeat
        } else if (msg.getType() == Message.HEARTBEAT){
            this.recvedHB[msg.getEmitter()] = (int)CommonState.getTime();
            Message chkm = new Message(Message.CHECKTM, "", msg.getEmitter());
            EDSimulator.add(CommonState.getTime() - this.recvedHB[msg.getEmitter()] + AramConfig.getHbtimeout(), chkm, this.getMyNode(), this.mypid);

        // Cas du message interne demandant de verifier si un noeud est fautif ou non
        } else if (msg.getType() == Message.CHECKTM){
            if (CommonState.getTime() - AramConfig.getHbtimeout() ==  this.recvedHB[msg.getEmitter()]){
                // Donc emitter est suspect
                Network.get(msg.getEmitter()).setFailState(Fallible.OK);
                Message wkm = new Message(Message.WAKEUP, "", this.nodeId);
                this.send(wkm, Network.get(msg.getEmitter()));
            }

        // Cas du message demandant le wakeup au noeud apres une detection de fail
        } else if (msg.getType() == Message.WAKEUP){
            this.nbWakeupRecvd++;
            if (this.nbWakeupRecvd == 3){
                Message failmsg = new Message(Message.FAILURE, "", this.nodeId);
                EDSimulator.add(0, failmsg, this.getMyNode(), this.mypid);
            }

        // Cas du message interne indiquant au noeud de se mettre à down
        } else if (msg.getType() == Message.GETDOWN){
            this.nbWakeupRecvd = 0;
            this.getMyNode().setFailState(Fallible.DOWN);
        }
    }

    // Retourne le noeud courant
    private Node getMyNode() {
        return Network.get(this.nodeId);
    }

    // Retourne l'etat courant du noeud
    public AppState getState(){
        return this.state;
    }

    public void setState(AppState as){
        this.state = as;
    }

    public String toString() {
        return "" + this.nodeId;
    }


}
