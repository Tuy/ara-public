package aram;

import com.mxgraph.model.mxCell;

import peersim.edsim.*;
import peersim.core.*;
import java.util.Arrays;

public class Checkpoint {

    // TODO: implements serializable

    private final int state;
    private final int countCheckpoints;
    private final int[] recvCounter;
    private final int[] sentCounter;
    private final int date;
    private final mxCell chk;


    Checkpoint(AppState as, int d) {
        this.state = as.getState();
        this.countCheckpoints = as.getCountCheckpoints();
        this.recvCounter = Arrays.copyOf(as.getRecvCounter(),as.getRecvCounter().length);
        this.sentCounter = Arrays.copyOf(as.getSentCounter(),as.getSentCounter().length);
        this.date = d;
        this.chk = null;
    }
    
    Checkpoint(AppState as, int d, mxCell chk) {
        this.state = as.getState();
        this.countCheckpoints = as.getCountCheckpoints();
        this.recvCounter = Arrays.copyOf(as.getRecvCounter(),as.getRecvCounter().length);
        this.sentCounter = Arrays.copyOf(as.getSentCounter(),as.getSentCounter().length);
        this.date = d;
        this.chk = chk;
    }

	public int getState() {
		return state;
	}
	public int getCountCheckpoints() {
		return countCheckpoints;
	}
	public int[] getRecvCounter() {
		return recvCounter;
	}
	public int[] getSentCounter() {
		return sentCounter;
	}
	public int getDate() {
		return date;
	}
	public mxCell getChk() {
		return chk;
	}
}

