package aram;

import peersim.edsim.*;
import peersim.core.*;
import peersim.config.*;

public class Controler implements peersim.core.Control {

	private int aramAppPid;
	private int step;
	private int countFailure;

    // Chargement du controller
	public Controler(String prefix) {
		this.aramAppPid = Configuration.getPid(prefix + ".aramAppProtocolPid");
		this.step = Configuration.getInt(prefix + ".step");
		this.countFailure = 0;
		AramConfig.loadConfig();
		System.out.println("Controler -> step: "+step);
	}

    // Fonction du controller
	public boolean execute() {
        // Si les conditions sont reunis on genere un fail
        if (countFailure < AramConfig.getMaxCountFailure() && CommonState.getTime() > 100){
            if (CommonState.r.nextDouble() < AramConfig.getFailureProb()){
                // Si les heartbeats sont activés on désactive le noeud
                if (AramConfig.getWithHB() == 1){
                    int whoDies = CommonState.r.nextInt(Network.size()-1) + 1;
                    Message gtd = new Message(Message.GETDOWN, "", whoDies);
                    EDSimulator.add(0, gtd, Network.get(whoDies), aramAppPid);
                // Sinon on lui envoie un message pour lui demander de lancer le recouvrement
                } else {
                    Message fmsg = new Message(Message.FAILURE, "", 0);
                    EDSimulator.add(0, fmsg, Network.get(CommonState.r.nextInt(Network.size()-1) + 1), aramAppPid);
                    this.countFailure++;
                }
            }
        }

        // On ralentit l'execution (utile avec le mode graphique)
		try {
			Thread.sleep(AramConfig.getPace());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

        // Si on a mis en pause, on s'arrete
		while(AramConfig.isPaused()){
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return false;
	}
}
