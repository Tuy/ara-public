package aram;


import peersim.edsim.*;
import peersim.core.*;
import peersim.config.*;

/*
   Module d'initialisation de helloWorld: 
Fonctionnement:
pour chaque noeud, le module fait le lien entre la couche transport et la couche applicative
ensuite, il fait envoyer au noeud 0 un message "Hello" a tous les autres noeuds
 */
public class Initializer implements peersim.core.Control {

	private int aramAppPid;

	public Initializer(String prefix) {
		// Recuperation du pid de la couche applicative
		this.aramAppPid = Configuration.getPid(prefix + ".aramAppProtocolPid");
		AramConfig.loadConfig();
	}

	public boolean execute() {
		int nodeNb;
		AramApp emitter, current;
		Node dest;
		Message incrMsg, cpMsg, shb;
		Graphics frame;

		// Recuperation de la taille du reseau
		nodeNb = Network.size();

		// Creation des messages
		incrMsg = new Message(Message.INCRSTATE,"", 0);
		cpMsg = new Message(Message.DOCHECKPOINT, "", 0);
        shb = new Message(Message.SENDHEARTBEATS, "", 0);

		if(AramConfig.isGraphicsEnabled()){
			frame = new Graphics(nodeNb-1, CommonState.getEndTime());
			cpMsg.setFrame(frame);
		}

		if (nodeNb < 1) {
			System.err.println("Network size is not positive");
			System.exit(1);
		}

		// Recuperation de la couche applicative de l'emetteur (le noeud 0)
		emitter = (AramApp)Network.get(0).getProtocol(this.aramAppPid);
		emitter.setTransportLayer(0);


		// Pour chaque noeud, on fait le lien entre la couche applicative et la couche transport
		// Puis on fait envoyer au noeud 0 un message qui lui indique qu'il faut s'incrementer
		for (int i = 1; i < nodeNb; i++) {
			dest = Network.get(i);
			current = (AramApp)dest.getProtocol(this.aramAppPid);
			current.setTransportLayer(i);
            // Demarrage des incrementations de state
			EDSimulator.add(CommonState.r.nextInt(AramConfig.getMaxIncr()-AramConfig.getMinIncr()) + AramConfig.getMinIncr(), incrMsg, Network.get(i), aramAppPid);
		    // Premier checkpoint
            EDSimulator.add(0, cpMsg, Network.get(i), aramAppPid);
            // Demarrage des checkpoints
			EDSimulator.add(CommonState.r.nextInt(AramConfig.getMaxBackupDelay()-AramConfig.getMinBackupDelay()) + AramConfig.getMinBackupDelay(), cpMsg, Network.get(i), aramAppPid);
            // Demarrage des heartbeats
			EDSimulator.add(0, shb, Network.get(i), aramAppPid);
		}

		if (nodeNb < 6) {
			System.err.println("At least 6 nodes are needed in the ring version");
			System.exit(1);	
		}	

		System.out.println("Initialization completed");
		return false;
	}
}
