package aram;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;

public class Graphics extends JFrame{	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static int width = 1200;
	private static int height;
	private static int size = 5;
	private static int timeStep = 10;
	
	
	private int procs;
	private int length;
	private int vspace;
	private int hspace;

	private String green = mxConstants.STYLE_FILLCOLOR + "=#00ff00";
	private String red = mxConstants.STYLE_FILLCOLOR + "=#ff0000";
	private String blue = mxConstants.STYLE_FILLCOLOR + "=#0000ff";
	private String orange = mxConstants.STYLE_FILLCOLOR + "=#ffa500";
	private String failed = mxConstants.STYLE_FILLCOLOR + "=#000000";
	private String arrow = mxConstants.STYLE_ENDARROW + "=" + mxConstants.NONE;
	private String label = mxConstants.STYLE_SHAPE + "=" + mxConstants.NONE;
	private String rollback = mxConstants.STYLE_STROKECOLOR + "=#ff0000";
	private String appli = mxConstants.STYLE_STROKECOLOR + "=#0000ff";

	private mxGraph graph;
	private mxGraphComponent graphComponent;
	private Object parent;
	private int yAxis[];
	private HashMap<String, mxCell> appMsgs;
	private HashMap<String, mxCell> rollMsgs;
	private int nxtTS;

	public Graphics(int nbProc, long endTime){
		vspace = 8*size;
		hspace = 50;
		length = (int) ((endTime*10)+hspace);
		procs = nbProc;
		nxtTS = hspace;

		height = (int) (vspace*(procs+5));
		graph = new mxGraph();
		parent = graph.getDefaultParent();
		Object start[] = new Object[procs];
		Object end[] = new Object[procs];
		yAxis = new int[procs];

		appMsgs = new HashMap<String, mxCell>();
		rollMsgs = new HashMap<String, mxCell>();

		AramConfig.loadConfig();
		graph.getModel().beginUpdate();

		try {

			for (int i=1; i<=procs; i++){
				yAxis[i-1] = i*vspace-size/2;
				
				graph.insertVertex(parent, "proc"+i, i, hspace-(size*2), i*vspace, 2, 2, label);
				
				start[i-1] = graph.insertVertex(parent, "start"+i, "", hspace,
						i*vspace, 0, 0);
				end[i-1] = graph.insertVertex(parent, "end"+i, "", length,
						i*vspace, 0, 0);
				graph.insertEdge(parent, "edge"+1, "", start[i-1], end[i-1], arrow);
			}

			printTimeAxis();
			printCaption();

		} finally {
			graph.setCellsMovable(false);
			graph.setCellsEditable(false);
			graph.setCellsResizable(false);
			graph.getModel().endUpdate();
		}
		
		graphComponent = new mxGraphComponent(graph);
		
		graphComponent.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {
			    if (e.getKeyCode() == KeyEvent.VK_SPACE){
			    	// Pause/Reprend la simulation avec la barre d'espace
			    	AramConfig.pause();
			    }
			    else if(e.getKeyCode() == KeyEvent.VK_UP){
			    	// Augmente la vitesse de simulation avec la fleche haut
			    	AramConfig.speedUp();
			    }
			    else if(e.getKeyCode() == KeyEvent.VK_DOWN){
			    	// Diminue la vitesse de simulation avec la fleche bas
			    	AramConfig.slowDown();
			    }
			    
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyTyped(KeyEvent e) {
			}
		});
		graphComponent.setAutoScroll(true);
		graphComponent.setFocusable(true);
		graphComponent.requestFocusInWindow();
		this.add(graphComponent);
		this.setTitle("Simulation Ara");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(0, 0);
		this.setSize(width, height);
		this.setVisible(true);

	}

	public mxCell addCheckpoint(int proc, int date, int chk){
		//int ext = (hspace+(date*timeStep)) - length;
		mxCell nChk =  (mxCell) graph.insertVertex(parent, proc+"check", "", 
				hspace+(date*timeStep), yAxis[proc-1], size, size, green);
		Object lb = graph.insertVertex(parent, "lbchk", chk, hspace+(date*timeStep)+1,
				yAxis[proc-1]-size, 2, 2, label);
		graphComponent.scrollCellToVisible(nChk);
		//if (ext > 0) extendEdges(ext+10);
		return nChk;
	}

	public mxCell addRollback(int proc, int date, int rbk){
		//int ext = (hspace+(date*timeStep)) - length;
		mxCell nRbk =  (mxCell) graph.insertVertex(parent, proc+"roll", "", 
				hspace+(date*timeStep), yAxis[proc-1], size, size, orange);
		Object lb = graph.insertVertex(parent, "lbrbk", rbk, hspace+(date*timeStep)+1,
				yAxis[proc-1]-size, 2, 2, label);
		graphComponent.scrollCellToVisible(nRbk);
		//if (ext > 0) extendEdges(ext+10);
		return nRbk;
	}
	
	public mxCell addState(int proc, int date, int state){
		mxCell nSt =  (mxCell) graph.insertVertex(parent, proc+"state", "", 
				hspace+(date*timeStep), yAxis[proc-1], size, size, blue);
		Object lb = graph.insertVertex(parent, "lbst", state, hspace+(date*timeStep)+1,
				yAxis[proc-1]-size, 2, 2, label);
		graphComponent.scrollCellToVisible(nSt);
		
		return nSt;
	}

	public void failed(int proc, int date){
		graph.insertVertex(parent, proc+"fail", "", hspace+(date*timeStep), yAxis[proc-1], size, size, failed);
		graph.insertVertex(parent, "lbfl", "F", hspace+(date*timeStep)+1,
				yAxis[proc-1]+(size*3), 4, 4, label);		
	}

	public mxCell addMessage(mxCell source, mxCell target, String style){
		return (mxCell) graph.insertEdge(parent, null, "", source, target, style);
	}

	public void disableCheckpoint(mxCell chk){
		chk.removeFromParent();
		//remove Msgs?
		chk.setStyle(red);
		graph.addCell(chk);
	}	

	public void sentAppMsg(int emitter, int receiver, int date, int cpt){
		mxCell v = (mxCell) graph.insertVertex(parent, null, "", hspace+(date*timeStep), yAxis[emitter-1]+size/2, 0, 0);
		addMsg(appMsgs, emitter, receiver, cpt, v);
	}

	public void recvdAppMsg(int emitter, int receiver, int date, int cpt){
		mxCell v1 = isExpected(appMsgs, emitter, receiver, cpt);
		if(v1 != null){
			mxCell v2 = (mxCell) graph.insertVertex(parent, null, "", hspace+(date*timeStep), yAxis[receiver-1]+size/2, 0, 0);
			addMessage(v1, v2, appli);
		}
	}

	public void sentRollMsg(int emitter, int receiver, int date){
		mxCell v = (mxCell) graph.insertVertex(parent, null, "", hspace+(date*timeStep), yAxis[emitter-1]+size/2, 0, 0);
		addMsg(rollMsgs, emitter, receiver, 0,  v);
	}

	public void recvdRollMsg(int emitter, int receiver, int date){
		mxCell v1 = isExpected(rollMsgs, emitter, receiver, 0);
		if(v1 != null){
			mxCell v2 = (mxCell) graph.insertVertex(parent, null, "", hspace+(date*timeStep), yAxis[receiver-1]+size/2, 0, 0);
			addMessage(v1, v2, rollback);
		}
	}

	private void extendEdges(int extension){

		for (int i=1; i<=procs+1; i++){
			Object start = graph.insertVertex(parent, "start"+i, "", length,
					i*vspace, 0, 0);
			Object end = graph.insertVertex(parent, "end"+i, "", length+extension,
					i*vspace, 0, 0);

			graph.insertEdge(parent, "edge"+1, "", start, end, arrow);			
		}
		insertTimeSteps(length, length+extension);
		length += extension;
		width = (int) (length+hspace);
		/*
		this.setPreferredSize(new Dimension(width, height));
		this.pack();
		 */
	}
	
	private void printCaption(){
		int y = (procs+2)*vspace;
		
		graph.insertVertex(parent, null, "Légende:", 2*hspace, y-10, 10, 10, label);

		String captGreen = "Checkpoint valide";
		String captBlue = "Changement d\'etat";
		String captOrange = "Rollback";
		String captRed = "Checkpoint invalide";
		String captFailed = "Processus failed";
		String captAppli = "Messages applicatifs";
		String captRollback = "Messages de rollback";
		
		graph.insertVertex(parent, null, "", 3*hspace, y, 10, 10, green);
		graph.insertVertex(parent, null, "", 3*hspace, y+20, 10, 10, blue);
		graph.insertVertex(parent, null, "", 6*hspace, y, 10, 10, orange);
		graph.insertVertex(parent, null, "", 6*hspace, y+20, 10, 10, red);
		graph.insertVertex(parent, null, "", 9*hspace, y+10, 10, 10, failed);


		Object a1 = graph.insertVertex(parent, null, "", 12*hspace, y, 0, 0);
		Object a2 = graph.insertVertex(parent, null, "", 12*hspace+40, y, 0, 0);
		graph.insertEdge(parent, null, "", a1, a2, appli);
		
		Object r1 = graph.insertVertex(parent, null, "", 12*hspace, y+20, 0, 0);
		Object r2 = graph.insertVertex(parent, null, "", 12*hspace+40, y+20, 0, 0);
		graph.insertEdge(parent, null, "", r1, r2, rollback);
		
		graph.insertVertex(parent, null, captGreen, 3*hspace+25+(captGreen.length()*2), y+2,
										10, 10, label);
		graph.insertVertex(parent, null, captBlue, 3*hspace+30+(captBlue.length()*2), y+22,
				10, 10, label);
		graph.insertVertex(parent, null, captOrange, 6*hspace+17+(captOrange.length()*2), y+2,
				10, 10, label);
		graph.insertVertex(parent, null, captRed, 6*hspace+25+(captRed.length()*2), y+22,
				10, 10, label);
		graph.insertVertex(parent, null, captFailed, 9*hspace+25+(captFailed.length()*2), y+12,
				10, 10, label);
		graph.insertVertex(parent, null, "F", 9*hspace+2, y+11+(size*3),
				4, 4, label);	

		graph.insertVertex(parent, null, captAppli, 12*hspace+65+(captFailed.length()*2), y,
				10, 10, label);
		graph.insertVertex(parent, null, captRollback, 12*hspace+67+(captFailed.length()*2), y+20,
				10, 10, label);
		

		
		graph.insertVertex(parent, null, "Commandes:        ", 19*hspace, y-10, 10, 10, label);
		graph.insertVertex(parent, null, "Pause     : barre espace", 21*hspace+8, y, 10, 10, label);
		graph.insertVertex(parent, null, "Ralentir  : flèche bas", 21*hspace, y+12, 10, 10, label);
		graph.insertVertex(parent, null, "Accélérer : flèche haut", 21*hspace+6, y+24, 10, 10, label);
	}

	private void printTimeAxis(){
		int y = (procs+1)*vspace;
		Object st = graph.insertVertex(parent, "startT", "", hspace,
				y, 0, 0);
		Object end = graph.insertVertex(parent, "endT", "", length,
				y, 0, 0);
		mxCell tAxis = (mxCell)graph.insertEdge(parent, "timeEdge", "", st, end, arrow);
		
		insertTimeSteps(hspace, length);
	}

	private void insertTimeSteps(int start, int end){
		int y = (procs+1)*vspace;
		
		while ((nxtTS >= start) && (nxtTS <= end)){
			Object top = graph.insertVertex(parent, "top", "", nxtTS,
					y-(size/2), 0, 0);
			Object bot = graph.insertVertex(parent, "top", "", nxtTS,
					y+(size/2), 0, 0);
			Object step = graph.insertEdge(parent, "step", "", top, bot, arrow);
			Object lab = graph.insertVertex(parent, "top", (nxtTS - hspace)/timeStep, nxtTS,
					y+(size*2), 2, 2, label);
			
			nxtTS += 10*timeStep;
		}

	}


	private void addMsg(HashMap<String, mxCell> hm, int em, int rc, int cpt, mxCell vertex){
		String str = em+"::"+rc/*+"::"+cpt*/;
		hm.put(str, vertex);
	}
	private mxCell isExpected(HashMap<String, mxCell> hm, int em, int rc, int cpt){
		mxCell source = null;
		String rmv = null;
		for (String key : hm.keySet()){
			String tab[] = key.split("::");
			if ((Integer.parseInt(tab[0]) == em) && (Integer.parseInt(tab[1]) == rc)/* && (Integer.parseInt(tab[2]) == cpt)*/){
				rmv = key;
				source = hm.get(key);
			}
		}
		if ((source != null) && (!rmv.equals(null))) hm.remove(rmv);
		return source;
	}
}
