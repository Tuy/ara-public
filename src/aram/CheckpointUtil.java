package aram;

import java.util.LinkedList;
import java.util.Arrays;

public class CheckpointUtil {
    // Fonctions créant un checkpoint à partir d'un état courant
	public static void doCheckPoint(LinkedList<Checkpoint> cplist, AppState as, int date, Graphics frame, int proc){
		as.incrCountCheckpoints();
		Checkpoint cp = new Checkpoint(as, date, frame.addCheckpoint(proc, date, as.getCountCheckpoints()));
		cplist.addLast(cp);
	}
	public static void doCheckPoint(LinkedList<Checkpoint> cplist, AppState as, int date){
		Checkpoint cp = new Checkpoint(as, date);
		cplist.addLast(cp);
		as.incrCountCheckpoints();
	}
	
    // Fonction permettant de rollback au dernier checkpoint
	public static void rollback(LinkedList<Checkpoint> cplist, AppState as){
		Checkpoint cp = cplist.getLast();
		as.setCountCheckpoints(cp.getCountCheckpoints());
		as.setRecvCounter(Arrays.copyOf(cp.getRecvCounter(), cp.getRecvCounter().length));
		as.setSentCounter(Arrays.copyOf(cp.getSentCounter(), cp.getSentCounter().length));
		as.setState(cp.getState());
	}
	
    // Fonctions permettant de rollback au checkpoint correspondant à l'etat recu
	public static void rollbackWhere(LinkedList<Checkpoint> cplist, AppState as, int emitter, int value, Graphics frame){
		for (int i=cplist.size()-1; i >= 0; i--){
			Checkpoint cp = cplist.get(i);
			if (cp.getRecvCounter()[emitter] > value){
				frame.disableCheckpoint(cplist.removeLast().getChk());				
			} else {
				rollback(cplist, as);
				break;
			}
		}
	}
	public static void rollbackWhere(LinkedList<Checkpoint> cplist, AppState as, int emitter, int value){
		for (int i=cplist.size()-1; i >= 0; i--){
			Checkpoint cp = cplist.get(i);
			if (cp.getRecvCounter()[emitter] > value){
				cplist.removeLast();
			} else {
				rollback(cplist, as);
				break;
			}
		}
	}
}
